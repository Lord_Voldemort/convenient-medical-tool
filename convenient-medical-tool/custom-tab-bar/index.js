// convenient-medical-tool/custom-tab-bar/index.js
const tabMap = require('../utils/constant').tabMap

Component({
  data: {
    isShowTab: [],
    selected: tabMap.homepage,
    color: '#7A7E83',
    selectedColor: '#1296db',
    list: [{
      pagePath: 'pages/patient/patientPage/patientPage',
      iconPath: '../images/homepage.png',
      selectedIconPath: '../images/homepageSelected.png',
      text: '首页'
    },
    {
      pagePath: 'pages/chatList/chatList',
      iconPath: '../images/chat.png',
      selectedIconPath: '../images/chatSelected.png',
      text: '聊天列表'
    },
    {
      pagePath: 'pages/patient/minePage/minePage',
      iconPath: '../images/mine.png',
      selectedIconPath: '../images/mineSelected.png',
      text: '我的'
    },
    {
      pagePath: 'pages/doctor/appointmentList/appointmentList',
      iconPath: '../images/appointment.png',
      selectedIconPath: '../images/appointmentSelected.png',
      text: '挂号列表'
    },
    {
      pagePath: 'pages/doctor/minePage/minePage',
      iconPath: '../images/mine.png',
      selectedIconPath: '../images/mineSelected.png',
      text: '我的'
    }
    ]
  },
  attached () {},
  methods: {
    switchTab (e) {
      const data = e.currentTarget.dataset
      const url = data.path
      wx.switchTab({
        url: url
      })
    }
  }
})