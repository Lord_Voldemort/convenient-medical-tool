// components/departmentCard/departmentCard.js
const departmentMap = require('../../utils/constant').departmentMap
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    departmentName: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    iconClass: '',
    color: 'rgb(7, 129, 230)'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    enterDepartment: function () {
      var detail = {departmentName: this.properties.departmentName}
      var options = {}
      this.triggerEvent('enterDepartment', detail, options)
    }
  },
  observers: {
    'departmentName': function (departmentName) {
      this.setData({
        iconClass: departmentMap[departmentName].iconClass,
        color: departmentMap[departmentName].color
      })
    }
  },
  options: {
    addGlobalClass: true
  }
})
