// components/doctorCard/doctorCard.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    id: {
      type: String,
      value: ''
    },
    name: {
      type: String,
      value: '张三'
    },
    job: {
      type: String,
      value: '主任医师'
    },
    rank: {
      type: String,
      value: '三甲'
    },
    hospital: {
      type: String,
      value: '泰达医院'
    },
    department: {
      type: String,
      value: '骨科'
    },
    goodRate: {
      type: Number,
      value: 1
    },
    comment: {
      type: Number,
      value: 4
    },
    patient: {
      type: Number,
      value: 657
    },
    speciality: {
      type: String,
      value: '心律失常诊断与治疗，高血压治疗'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    enterDoctor: function () {
      var detail = {id: this.properties.id}
      var options = {}
      this.triggerEvent("enterDoctor", detail, options)
    }
  }
})
