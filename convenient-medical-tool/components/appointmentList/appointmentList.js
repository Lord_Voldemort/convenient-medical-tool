// components/appointmentList/appointmentList.js
Component({
  lifetimes: {
    attached: function () {
      this.getDepartmentInfo()
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {
    departmentId: {
      type: String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    doctorList: [],
    department: '',
    hospital: ''
  },
  observers: {
    'departmentId': function () {
      this.getDepartmentInfo()
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getDoctorList: function () {
      const that = this
      wx.cloud.callFunction({
        name: 'getDoctorToAppointment',
        data: {
          department: this.data.department,
          hospital: this.data.hospital
        },
        success: function (res) {
          that.setData({
            doctorList: res.result.doctorList
          })
        }
      })
    },
    getDepartmentInfo: function () {
      const that = this
      wx.cloud.callFunction({
        name: 'getDepartmentInfo',
        data: {
          departmentId: that.properties.departmentId
        },
        success: function (res) {
          that.setData({
            department: res.result.department,
            hospital: res.result.hospital
          }, function () {
            that.getDoctorList()
          })
        }
      })
    },
    getAppointment: function (e) {
      var detail = {doctorId: e.currentTarget.dataset.id}
      var options = {}
      this.triggerEvent('getAppointment', detail, options)
    }
  }
})
