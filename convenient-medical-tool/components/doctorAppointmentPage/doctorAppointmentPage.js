// components/doctorAppointmentPage/doctorAppointmentPage.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    doctorId: {
      type: String
    },
    date: {
      type: String
    },
    tomorrowDate: {
      type: String
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    selectedPeriod: 'am',
    selectedDate: '',
    name: '',
    department: '',
    job: '',
    hospital: ''
  },
  observers: {
    'doctorId': function (doctorId) {
      this.getDoctorInfo(doctorId)
    },
    'date': function (date) {
      this.setData({
        selectedDate: date
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getDoctorInfo: async function (doctorId) {
      let res = await wx.cloud.callFunction({
        name: 'getDoctorInfo',
        data: {
          doctorId: doctorId
        }
      })
      this.setData({
        department: res.result.department,
        hospital: res.result.hospital,
        job: res.result.job,
        name: res.result.name
      })
    },
    chooseDateAndPeriod: function (e) {
      var selectDate = e.currentTarget.dataset.date
      var selectPeriod = e.currentTarget.dataset.period
      this.setData({
        selectedDate: selectDate,
        selectedPeriod: selectPeriod
      })
    }
  }
})
