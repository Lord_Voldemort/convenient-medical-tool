// components/chatCard/chatCard.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: 'title'
    },
    time: {
      type: Date,
      value: '0:00'
    },
    message: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChatTap: function () {
      var detail = {}
      var options = {}
      this.triggerEvent("enterChat", detail, options)
    }
  }
})
