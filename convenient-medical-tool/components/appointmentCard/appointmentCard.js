// components/appointmentCard/appointmentCard.js
const shiftMap = require('../../utils/constant').shiftMap
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    date: {
      type: String
    },
    period: {
      type: String
    },
    doctorId: {
      type: String
    },
    department: {
      type: String
    }
  },
  observers: {
    'date,period': function () {
      this.getDoctorArrangement()
    }
  },
  /**
   * 组件的初始数据
   */
  data: {
    arrangeList: [],
    account_taken: 0,
    account_remain: 0
  },
  options: {
    addGlobalClass: true
  },
  /**
   * 组件的方法列表
   */
  methods: {
    confirm: function (e) {
      const that = this
      var arrangementId = e.currentTarget.dataset.arrangementid
      var patientId = ''
      let openid = wx.getStorageSync('openid')
      wx.cloud.callFunction({
        name: 'getPatientID',
        data: {
          openid: openid
        },
        success: function (res) {
          patientId = res.result.idcard
          wx.showModal({
            content: '是否预约',
            success: res => {
              if (res.confirm) {
                that.createAppointment(arrangementId, patientId)
              }
            }
          })
          
        }
      })
    },
    createAppointment: function (arrangementId, patientId) {
      wx.cloud.callFunction({
        name: 'createAppointment',
        data: {
          arrangementId: arrangementId,
          doctorId: this.data.doctorId,
          patientId: patientId
        },
        success (res) {
          if (res.result.errMsg === 'collection.add:ok') {
            wx.showToast({
              title: '预约成功',
              icon: 'success',
              duration: 2000
            })
          }
        }
      })
    },
    getDoctorArrangement: function () {
      const that = this
      wx.cloud.callFunction({
        name: 'getDoctorArrangement',
        data: {
          doctorId: this.properties.doctorId,
          date: this.properties.date,
          period: this.properties.period
        },
        success: function (res) {
          that.shiftMapChange(res.result.arrangeList)
        }
      })
    },
    shiftMapChange: function (arrangeList) {
      for (var arrange of arrangeList)
      {
        arrange.shift = shiftMap[this.data.period][arrange.shift]
      }
      this.setData({
        arrangeList: arrangeList
      })
    } 
  }
})
