// components/hospitalCard/hospitalCard.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: '医院'
    },
    location: {
      type: Object,
      value: {}
    },
    address: {
      type: String,
      value: '南开大学'
    },
    distance: {
      type: Number,
      value: 0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onHospitalTap: function () {
      var detail = {
        title: this.properties.title
      }
      var options = {}
      this.triggerEvent("enterHospital", detail, options)
    }
  }
})
