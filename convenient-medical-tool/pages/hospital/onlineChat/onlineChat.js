// pages/hospital/onlineChat/onlineChat.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '',
    surgeryList: [],
    internalList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.setNavigationBarTitle({
      title: '在线咨询'
    })
    this.setData({
      title: options.title
    }, function () {
      this.getDepartment()
    } )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getDepartment: function () {
    const that = this
    const db = wx.cloud.database()
    db.collection('departments').where({
      hospital: that.data.title
    }).get({
      success: function (res) {
        if (res.data.length != 0) {
          var surgeryList = []
          var internalList = []
          for (var item of res.data)
          {
            if ('内科'.localeCompare(item.variety) == 0) {
              internalList.push(item.department)
            } else if ('外科'.localeCompare(item.variety) == 0) {
              surgeryList.push(item.department)
            }
          }
          that.setData({
            surgeryList: surgeryList,
            internalList: internalList
          })
        }
      }
    })
  },
  enterDepartment: function (e) {
    wx.navigateTo({
      url: '../specialistList/specialistList?departmentName=' + e.detail.departmentName + '&title=' + this.data.title
    })
  }
})