// pages/hospital/hospitalPage/hospitalPage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '泰达医院',
    hospital_rank: '',
    appointment_account: 0,
    consult_account: 0,
    address: '',
    doctors_for_appointment: 3,
    doctors_for_consult: 3
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.setNavigationBarTitle({
      title: '医院主页'
    })
    this.setData({
      title: options.title
    }, function () {
      this.getHospitalInfo()
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getHospitalInfo: function () {
    const that = this
    const db = wx.cloud.database()
    db.collection('hospitals').where({
      name: that.data.title
    }).get({
      success: function (res) {
        that.setData({
          hospital_rank: res.data[0].rank,
          appointment_account: res.data[0].appointment,
          consult_account: res.data[0].consult,
          address: res.data[0].address
        })
      }
    })
  },
  onlineChat: function () {
    wx.navigateTo({
      url: '../onlineChat/onlineChat?title=' + this.data.title
    })
  },
  getAppointment: function () {
    wx.navigateTo({
      url: '../appointmentPage/appointmentPage?title=' + this.data.title
    })
  }
})