// pages/hospital/appointmentPage/appointmentPage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    departmentList: [],
    selected: "1",
    selectedDoctorId: '',
    isShow: false,
    customStyle: 'height: 70vh;',
    date: '',
    tomorrowDate: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.setNavigationBarTitle({
      title: '预约挂号'
    })
    this.getDepartmentList(options.title)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  chooseDepartment: function (e) {
    const departmentId = e.currentTarget.dataset.departmentid
    this.setData({
      selected: departmentId
    })
  },
  getDepartmentList: function (title) {
    const that = this
    wx.cloud.callFunction({
      name: 'getDepartmentList',
      data: {
        hospital: title
      },
      success: function (res) {
        that.setData({
          departmentList: res.result.departmentList
        })
      }
    })
  },
  getAppointment: function (e) {
    this.setData({
      selectedDoctorId: e.detail.doctorId,
      isShow: true
    })
  },
  beforeEnter: function () {
    this.getDate()
  },
  getDate: function () {
    var d = new Date()
    var year = d.getFullYear()
    var month = d.getMonth() + 1
    var day = d.getDate()
    
    month = (month<10)?("0"+month):month
    day = (day<10)?("0"+day):day

    var date = year + '-' + month + '-' + day
    
    d.setDate(d.getDate() + 1)
    year = d.getFullYear()
    month = d.getMonth() + 1
    day = d.getDate()

    month = (month<10)?("0"+month):month
    day = (day<10)?("0"+day):day
    
    var tomorrowDate = year + '-' + month + '-' + day

    this.setData({
      date: date,
      tomorrowDate: tomorrowDate
    })
  },
})