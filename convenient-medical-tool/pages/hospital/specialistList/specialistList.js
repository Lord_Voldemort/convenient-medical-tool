// pages/hospital/specialistList/specialistList.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    departmentName: '',
    hospital: '',
    doctorList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const that = this
    this.setData({
      departmentName: options.departmentName,
      hospital: options.title
    }, function () {
      wx.setNavigationBarTitle({
        title: that.data.departmentName,
      })
      that.getDoctors()
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getDoctors: function () {
    const that = this
    const db = wx.cloud.database()
    db.collection('doctors').where({
      hospital: that.data.hospital,
      department: that.data.departmentName
    }).get({
      success: function (res) {
        if (res.data.length != 0) {
          var tempList = []
          for (var item of res.data)
          {
            tempList.push({
              id: item.id,
              name: item.name,
              job: item.job,
              hospital: that.data.hospital,
              departmentName: that.data.departmentName,
            })
          }
          that.setData({
            doctorList: tempList
          })
        }
      }
    })
  },
  enterDoctor: function (e) {
    wx.navigateTo({
      url: '../../im/room/room?doctorId=' + e.target.id
    })
  }
})