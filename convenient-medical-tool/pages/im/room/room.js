const app = getApp()
 
Page({
  data: {
    avatarUrl: './user-unlogin.png',
    userInfo: null,
    logged: false,
    takeSession: false,
    requestResult: '',
    // chatRoomEnvId: 'release-f8415a',
    chatRoomCollection: 'chatroom',
    chatRoomGroupId: '',
    patientId: '',
    doctorId: '',
    chatRoomGroupName: '聊天室',

    // functions for used in chatroom components
    onGetUserInfo: null,
    getOpenID: null,
  },

  onLoad: function(options) {

    // 设置导航栏title
    wx.setNavigationBarTitle({
      title: '在线咨询',
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              this.setData({
                avatarUrl: res.userInfo.avatarUrl,
                userInfo: res.userInfo
              })
            }
          })
        }
      }
    })

    const actor = wx.getStorageSync('actor')
    const that = this
    if (actor == 'doctor') {
      this.setData({
        onGetUserInfo: this.onGetUserInfo,
        getOpenID: this.getOpenID,
        chatRoomGroupId: options.openid + '/' + options.doctorId,
        patientId: options.openid,
        doctorId: options.doctorId
      })
    } else if (actor == 'patient') {
      const openid = wx.getStorageSync('openid')
      this.setData({
        onGetUserInfo: this.onGetUserInfo,
        getOpenID: this.getOpenID,
        chatRoomGroupId: openid + '/' + options.doctorId,
        patientId: openid,
        doctorId: options.doctorId
      })
    }  
  },

  getOpenID: async function() {
    if (this.openid) {
      return this.openid
    }

    const { result } = await wx.cloud.callFunction({
      name: 'login',
    })

    return result.openid
  },

  onGetUserInfo: function(e) {
    if (!this.logged && e.detail.userInfo) {
      this.setData({
        logged: true,
        avatarUrl: e.detail.userInfo.avatarUrl,
        userInfo: e.detail.userInfo
      })
    }
  },

  onShareAppMessage() {
    return {
      title: '即时通信 Demo',
      path: '/pages/im/room/room',
    }
  },
})
