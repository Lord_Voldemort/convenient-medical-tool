// convenient-medical-tool/pages/loginPage/patientLogin/patientLogin.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    idcard: '',
    phone: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.cloud.callFunction({
      name: 'checkLoginActive',
      data: {
        actor: 'patient'
      },
      success: function (res) {
        if (res.result.msg === 'NOT EXPIRE') {
          wx.setStorageSync('actor', 'patient')
          wx.setStorageSync('openid', res.result.openid)
          wx.switchTab({
            url: '../../patient/patientPage/patientPage'
          })
        }
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onLoad()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindNameInput: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
  bindIdcardInput: function (e) {
    this.setData({
      idcard: e.detail.value
    })
  },
  bindPhoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  goBack: function () {
    wx.navigateTo({
      url: '../../homePage/homePage',
    })
  },
  patientLogin: function () {
    wx.cloud.callFunction({
      name: 'patientLogin',
      data: {
        name: this.data.name,
        idcard: this.data.idcard,
        phone: this.data.phone,
        actor: 'patient'
      },
      success: function (res) {
        wx.setStorageSync('actor', 'patient')
        wx.setStorageSync('openid', res.result.openid)
        wx.switchTab({
          url: '../../patient/patientPage/patientPage'
        })
      }
    })

  }
})