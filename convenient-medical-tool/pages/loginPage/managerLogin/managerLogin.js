// convenient-medical-tool/pages/loginPage/managerPage/managerLogin.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    account: '',
    pwd: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindAccountInput: function (e) {
    this.setData({
      account: e.detail.value
    })
  },
  bindPwdInput: function (e) {
    this.setData({
      pwd: e.detail.value
    })
  },
  goBack: function () {
    wx.navigateTo({
      url: '../../homePage/homePage',
    })
  },
  managerLogin: function () {
    if (this.data.account && this.data.pwd) {
      wx.cloud.callFunction({
        name: 'managerLogin',
        data: {
          id: this.data.account,
          password: this.data.pwd
        },
        success: function (res) {
          if (res.result.msg === 'EXIST') {
            wx.showToast({
              title: '登录成功',
              icon: 'success',
              duration: 2000
            })
            wx.navigateTo({
              url: '../../manager/managerPage/managerPage',
            })
          } else if (res.result.msg === 'NOT EXIST') {
            wx.showToast({
              title: '登录失败',
              icon: 'error',
              duration: 2000
            })
          }
        }
      })
      
    }
  }
})