// convenient-medical-tool/pages/doctor/appointmentList/appointmentList.js
const shiftMap = require('../../../utils/constant').shiftMap
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appointmentList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '挂号列表'
    })
    this.messageWatch()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const tabMap = require('../../../utils/constant').tabMap
    const actorMap = require('../../../utils/constant').actorMap
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: tabMap.appointment
      })
    }
    this.getTabBar().setData({
      isShowTab: actorMap['doctor']
    })
    // this.getAppointmentList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getAppointmentList: function (appointmentList) {
    console.log('origin', appointmentList)
    const that = this
    wx.cloud.callFunction({
      name: 'getAppointmentList',
      data: {
        appointmentList: appointmentList
      },
      success: function (res) {
        var newList = that.shiftMapChange(res.result)
        var tempList = that.data.appointmentList
        tempList.push.apply(tempList, newList)
        that.setData({
          appointmentList: tempList
        })
      }
    })
  },
  shiftMapChange: function (appointmentList) {
    for (var appointment of appointmentList)
    {
      appointment.shift = shiftMap[appointment.period][appointment.shift]
      appointment.period = appointment.period === 'am' ? '上午' : '下午'
    }
    return appointmentList
  },
  async messageWatch() {
    const that = this
    const db = wx.cloud.database()
    var doctorId = wx.getStorageSync('doctorId')

    wx.cloud.callFunction({
      name: 'getDoctorID',
      data: {
        doctorId: doctorId
      },
      success: function (res) {
        doctorId = res.result.doctorId
        console.log(doctorId)
        this.messageListener = db.collection('appointments')
        .where({
          doctorId: doctorId
        })
        .watch({
          onChange: that.onRealTimeAppointmentSnapshot.bind(that),
          onError: function (error) {
            console.log(error)
          }
        })
      }
    })
    
  },
  onRealTimeAppointmentSnapshot: function (snapshot) {
    const that = this
    console.log(snapshot)
    if (snapshot.type === 'init') {
      const appointmentList = snapshot.docs
      that.getAppointmentList(appointmentList)
    } else {
      for (var docChange of snapshot.docChanges)
      {
        switch (docChange.queueType) {
          case 'enqueue' : {
            that.getAppointmentList([docChange.doc])
          }
        }
      }
    }
  }
})