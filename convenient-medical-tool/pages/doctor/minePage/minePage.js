// convenient-medical-tool/pages/doctor/minePage/minePage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const tabMap = require('../../../utils/constant').tabMap
    const actorMap = require('../../../utils/constant').actorMap
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: tabMap.mine_doctor
      })
    }
    this.getTabBar().setData({
      isShowTab: actorMap['doctor']
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  confirmSignOut: function () {
    const that = this
    wx.showModal({
      content: '是否要登出账号',
      success (res) {
        if (res.confirm) {
          that.signOut()
        }
      }
    })
    
  },
  signOut: function () {
    wx.cloud.callFunction({
      name: 'signOut',
      data: {
        actor: 'doctor'
      },
      success: function () {
        wx.navigateTo({
          url: '../../homePage/homePage',
        })
      }
    })
  }
})