// pages/manager/setArrangement/setArrangement.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    date: '',
    multiArray: [['am', 'pm'], ['8:30-9:30', '9:30-10:30', '10:30-11:30']],
    objectMultiArray: [
      [
        {
          id: 0,
          name: 'am'
        },
        {
          id: 1,
          name: 'pm'
        }
      ], [
        {
          id: 0,
          name: '8:30-9:30'
        },
        {
          id: 1,
          name: '9:30-10:30'
        },
        {
          id: 2,
          name: '10:30-11:30'
        }
      ]
    ],
    multiIndex: [0, 0],
    doctorId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  bindDateChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      date: e.detail.value
    })
  },
  bindMultiPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      multiIndex: e.detail.value
    })
  },
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = ['8:30-9:30', '9:30-10:30', '10:30-11:30']
            break;
          case 1:
            data.multiArray[1] = ['14:30-15:30', '15:30-16:30', '16:30-17:30']
            break;
        }
        data.multiIndex[1] = 0;
        break;
    }
    console.log(data.multiIndex);
    this.setData(data);
  },
  bindDoctorIdInput: function (e) {
    this.setData({
      doctorId: e.detail.value
    })
  },
  submit: function () {
    if (this.data.doctorId != '') {
      wx.cloud.callFunction({
        name: 'addArrangement',
        data: {
          date: this.data.date,
          period: this.data.multiArray[0][this.data.multiIndex[0]],
          shift: this.data.multiArray[1][this.data.multiIndex[1]],
          doctorId: this.data.doctorId
        },
        success: function (res) {
          wx.showToast({
            title: '添加成功',
            icon: 'success',
            duration: 2000
          })
        }
      })
    }
  }
})