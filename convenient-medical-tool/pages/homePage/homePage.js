const app = getApp()

Page ({
  data: {

  },

  doctorLogin: function () {
    wx.navigateTo({
      url: '../loginPage/doctorLogin/doctorLogin',
    })
  },
  patientLogin: function () {
    wx.navigateTo({
      url: '../loginPage/patientLogin/patientLogin',
    })
  },
  managerLogin: function () {
    wx.navigateTo({
      url: '../loginPage/managerLogin/managerLogin',
    })
  }
})