// convenient-medical-tool/pages/patientPage/patientPage.js
const QQMapWX = require(
  '../../../libs/qqmap-wx-jssdk.js'
  )
var qqmapsdk
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isAllowedToGetLocation: true,
    province: '',
    city: '',
    district: '',
    latitude: 0,
    longitude: 0,
    hospitalList: [],
    distance: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.qqmapsdk = new QQMapWX({
      key: 'UHDBZ-J3WW3-MPS3L-3UQQF-B7AP7-ZSBVJ'
    })
    wx.setNavigationBarTitle({
      title: '患者首页'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const tabMap = require('../../../utils/constant').tabMap
    const actorMap = require('../../../utils/constant').actorMap
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: tabMap.homepage
      })
    }
    this.getTabBar().setData({
      isShowTab: actorMap['patient']
    })
    this.getLocation()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getLocation: function () {
    let that = this
    wx.getLocation({
      type: 'gcj02',
      success (response) {
        // console.log(response)
        // 获取当前位置的经纬度
        that.setData({
          latitude: response.latitude,
          longitude: response.longitude
        }, function () {
          // 异步等待，获取医院信息
          that.getHospital()
        })
        // 逆向解析经纬度，获取城市信息
        that.qqmapsdk.reverseGeocoder({
          location: {
            latitude: response.latitude,
            longitude: response.longitude
          },
          success: inner_response => {
            that.setData({
              isAllowedToGetLocation: true,
              province: inner_response.result.address_component['province'],
              city: inner_response.result.address_component['city'],
              district: inner_response.result.address_component['district']
            })
          },
          fail: inner_response => {
            that.setData({
              isAllowedToGetLocation: false
            })
          }
        })
      },
      fail (response) {
        that.setData({
          isAllowedToGetLocation: false
        })
      }
    })
  },
  getLatAndLng: function () {
    wx.chooseLocation({
      success: response => {
        console.log(response)
        that.qqmapsdk.reverseGeocoder({
          location: {
            latitude: response.latitude,
            longitude: response.longitude
          },
          success: inner_response => {
            console.log(inner_response)
          }
        })
      }
    })
  },
  getHospital: function () {
    const that = this
    const db = wx.cloud.database()
    // 获取所有医院信息
    db.collection('hospitals').get({
      success: function (res) {
        if (res.data.length != 0) {
          var tempList = []
          var fromDest = '' + that.data.latitude + ',' + that.data.longitude
          var toDest = ''
          for (var item in res.data)
          {
            toDest += res.data[item].location.latitude + ',' + res.data[item].location.longitude
            if (item != res.data.length - 1) toDest += ';'
          }

          // 计算当前位置到各医院的距离
          that.qqmapsdk.calculateDistance({
            from: fromDest,
            to: toDest,
            mode: 'driving',
            success: function (response) {
              var distance = []
              for (var item of response.result.elements)
              {
                distance.push(item.distance / 1000)
              }
              // 构造hospitalList列表
              for(var item in res.data)
              {
                tempList.push({
                  title: res.data[item]['name'],
                  rank: res.data[item]['rank'],
                  address: res.data[item]['address'],
                  distance: distance[item]
                })
              }
              that.setData({
                hospitalList: tempList
              })
            },
            fail: function(error) {
              console.error(error)
            }
          })
          
        }
      }
    })
  },
  enterHospital: function (e) {
    wx.navigateTo({
      url: '../../hospital/hospitalPage/hospitalPage?title=' + e.detail.title
    })
  }
})