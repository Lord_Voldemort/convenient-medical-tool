// pages/patient/personalInfoPage/personalInfoPage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    phone: '',
    idcard: '',
    tempName: '',
    tempPhone: '',
    isModifyName: false,
    isModifyPhone: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.setNavigationBarTitle({
      title: '个人信息'
    })
    this.getPatientInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  getPatientInfo: function () {
    const that = this
    wx.cloud.callFunction({
      name: 'getPatientInfo',
      success: function (res) {
        that.setData({
          name: res.result.name,
          phone: res.result.phone,
          idcard: res.result.idcard,
          isModifyName: false,
          isModifyPhone: false
        })
      }
    })
  },
  bindNameInput: function (e) {
    this.setData({
      tempName: e.detail.value
    })
  },
  bindPhoneInput: function (e) {
    this.setData({
      tempPhone: e.detail.value
    })
  },
  modifyName: function () {
    this.setData({
      isModifyName: true
    })
  },
  modifyPhone: function () {
    this.setData({
      isModifyPhone: true
    })
  },
  submitName: function () {
    const that = this
    wx.showModal({
      content: '确定提交修改吗？',
      success (res) {
        if (res.cancel) {
          that.setData({
            isModifyName: false,
            tempName: ''
          })
        } else if (res.confirm) {
          that.modifyInformation('name', that.data.tempName)
        }
      }
    })
  },
  submitPhone: function () {
    const that = this
    wx.showModal({
      content: '确定提交修改吗？',
      success (res) {
        if (res.cancel) {
          that.setData({
            isModifyPhone: false,
            tempPhone: ''
          })
        } else if (res.confirm) {
          that.modifyInformation('phone', that.data.tempPhone)
        }
      }
    })
  },
  modifyInformation: function (attribute, value) {
    const that = this
    wx.cloud.callFunction({
      name: 'modifyInformation',
      data: {
        attribute: attribute,
        value: value
      },
      success: function (res) {
        if (res.result.msg === 'UPDATE FAIL') {
          wx.showToast({
            title: '修改失败',
            icon: 'error',
            duration: 2000
          })
        } else if (res.result.msg === 'SUCCESS') {
          wx.showToast({
            title: '修改成功',
            icon: 'success',
            duration: 2000
          })
        }
        that.getPatientInfo()
      }
    })
  },
})