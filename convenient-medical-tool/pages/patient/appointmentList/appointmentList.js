// pages/patient/appointmentList/appointmentList.js
const shiftMap = require('../../../utils/constant').shiftMap
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appointmentList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.setNavigationBarTitle({
      title: '挂号列表'
    })
    this.messageWatch()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  async messageWatch() {
    const that = this
    const db = wx.cloud.database()
    var openid = wx.getStorageSync('openid')

    wx.cloud.callFunction({
      name: 'getPatientID',
      data: {
        openid: openid
      },
      success: function (res) {
        let patientId = res.result.idcard
        console.log(patientId)
        this.messageListener = db.collection('appointments')
        .where({
          patientId: patientId
        })
        .watch({
          onChange: that.onRealTimeAppointmentSnapshot.bind(that),
          onError: function (error) {
            console.log(error)
          }
        })
      }
    })
    
  },
  onRealTimeAppointmentSnapshot: function (snapshot) {
    const that = this
    console.log(snapshot)
    if (snapshot.type === 'init') {
      const appointmentList = snapshot.docs
      that.getAppointmentList(appointmentList)
    } else {
      for (var docChange of snapshot.docChanges)
      {
        switch (docChange.queueType) {
          case 'enqueue' : {
            that.getAppointmentList([docChange.doc])
          }
        }
      }
    }
  },
  getAppointmentList: function (appointmentList) {
    console.log('origin', appointmentList)
    const that = this
    wx.cloud.callFunction({
      name: 'getAppointmentList',
      data: {
        appointmentList: appointmentList
      },
      success: function (res) {
        var newList = that.shiftMapChange(res.result)
        var tempList = that.data.appointmentList
        tempList.push.apply(tempList, newList)
        that.setData({
          appointmentList: tempList
        })
      }
    })
  },
  shiftMapChange: function (appointmentList) {
    for (var appointment of appointmentList)
    {
      appointment.shift = shiftMap[appointment.period][appointment.shift]
      appointment.period = appointment.period === 'am' ? '上午' : '下午'
    }
    return appointmentList
  }
})