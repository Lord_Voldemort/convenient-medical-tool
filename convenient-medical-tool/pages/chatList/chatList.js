
// convenient-medical-tool/pages/doctor/chatList/chatList.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chatList: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '聊天列表'
    })
    this.messageWatch()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const tabMap = require('../../utils/constant').tabMap
    const actorMap = require('../../utils/constant').actorMap
    const actor = wx.getStorageSync('actor')
    if (typeof this.getTabBar === 'function' &&
      this.getTabBar()) {
      this.getTabBar().setData({
        selected: tabMap.chat
      })
    }
    this.getTabBar().setData({
      isShowTab: actorMap[actor]
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  enterChat: function (e) {
    const actor = wx.getStorageSync('actor')
    var doctorId = ''
    var patientId = ''
    if (actor === 'patient') {
      doctorId = e.currentTarget.dataset.doctorid
      patientId = wx.getStorageSync('openid')
    } else {
      doctorId = wx.getStorageSync('doctorId')
      patientId = e.currentTarget.dataset.patientid
    }
    console.log(doctorId, patientId)
    wx.navigateTo({
      url: '../im/room/room?openid=' + patientId + '&doctorId=' + doctorId
    })
  },

  async messageWatch() {
    const db = wx.cloud.database()
    const actor = wx.getStorageSync('actor')
    var doctorId = ''
    var patientId = ''
    if (actor === 'doctor') {
      doctorId = wx.getStorageSync('doctorId')
      console.warn(`开始监听`)
      this.messageListener = db.collection('chatroom')
      .where({
        doctorId: doctorId
      })
      .orderBy('sendTimeTS', 'asc')
      .watch({
        onChange: this.onRealTimeMessageSnapshot.bind(this),
        onError: function (error) {
          console.log(error)
        }
      })
    } else {
      patientId = wx.getStorageSync('openid')
      console.warn(`开始监听`)
      this.messageListener = db.collection('chatroom')
      .where({
        patientId: patientId
      })
      .orderBy('sendTimeTS', 'asc')
      .watch({
        onChange: this.onRealTimeMessageSnapshot.bind(this),
        onError: function (error) {
          console.log(error)
        }
      })
    }
    
  },

  onRealTimeMessageSnapshot: function (snapshot) {
    const that = this
    console.log(snapshot)
    if (snapshot.type === 'init') {
      const message = snapshot.docs
      this.getChatList(message)
    } else {
      for (var docChange of snapshot.docChanges)
      {
        switch (docChange.queueType) {
          case 'enqueue': {
            const actor = wx.getStorageSync('actor')
            var actorId = actor === 'doctor' ? docChange.doc.patientId : docChange.doc.doctorId
            if (this.data.chatList.hasOwnProperty(actorId)) {
              if (docChange.doc.msgType === 'image') {
                this.setData({
                  [`chatList.${actorId}.textContent`]: '[图片]'
                })
              } else {
                this.setData({
                  [`chatList.${actorId}.textContent`]: docChange.doc.textContent
                })
              }
            } else {
              this.getChatList([docChange.doc])
            }
          }
        }
      }
    }
  },
  getChatList: function (message) {
    console.log('message', message)
    const actor = wx.getStorageSync('actor')
    wx.cloud.callFunction({
      name: 'getChatList',
      data: {
        message: message,
        actor: actor
      }
    }).then((res) => {
      var mergeChat = Object.assign(res.result, this.data.chatList)
      this.setData({
        chatList: mergeChat
      })
    })
  }
})