module.exports = {
  tabMap: {
    homepage: 0,
    chat: 1,
    mine_patient: 2,
    appointment: 3,
    mine_doctor: 4
  },
  actorMap: {
    patient: [true, true, true, false, false],
    doctor: [false, true, false, true, true]
  },
  departmentMap: {
    '骨科': {iconClass: 'bone', color: 'rgb(214, 195, 88)'},
    '普通外科': {iconClass: 'surgery', color: 'rgb(81, 180, 247)'},
    '泌尿外科': {iconClass: 'urinary', color: 'rgb(44, 26, 124)'},
    '胸外科': {iconClass: 'chest', color: 'rgb(33, 58, 173)'},
    '神经外科': {iconClass: 'neuro', color: 'rgb(230, 128, 13)'},
    '皮肤科': {iconClass: 'skin', color: 'rgb(162, 170, 53)'},
    '口腔科': {iconClass: 'mouth', color: 'rgb(12, 103, 206)'},
    '心脏内科': {iconClass: 'heart', color: 'rgb(238, 25, 25)'},
    '内分泌科': {iconClass: 'endocrine', color: 'rgb(233, 111, 11)'},
    '心衰科': {iconClass: 'heart-failure', color: 'rgb(172, 10, 10)'},
    '消化内科': {iconClass: 'digest', color: 'rgb(53, 12, 235)'}
  },
  periodMap: {
    'TODAY_MORNING': '0',
    'TODAY_AFTERNOON': '1',
    'TOMORROW_MORNING': '2',
    'TOMORROW_AFTERNOON': '3'
  },
  reversePeriodMap: {
    '0': 'TODAY_MORNING',
    '1': 'TODAY_AFTERNOON',
    '2': 'TOMORROW_MORNING',
    '3': 'TOMORROW_AFTERNOON'
  },
  shiftMap: {
    'am': {
      '0': '8:30-9:30',
      '1': '9:30-10:30',
      '2': '10:30-11:30'
    },
    'pm': {
      '0': '14:30-15:30',
      '1': '15:30-16:30',
      '2': '16:30-17:30'
    }
  }
}