// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
async function getPatientInfo (res) {
  return {
    name: res.data[0].name,
    phone: res.data[0].phone,
    idcard: res.data[0].idcard
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  
  let res = await db.collection('patients')
  .where({
    openid: openid
  }).get()

  let ans = await getPatientInfo(res)
  return ans
}