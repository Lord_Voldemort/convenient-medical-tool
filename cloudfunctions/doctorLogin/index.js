// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const textContent = cloud.getWXContext()
  const openid = textContent.OPENID
  let res
  try {
    res = await db.collection('doctors')
    .where({
      id: event.id,
      password: event.password
    }).get()

    if (res.data.length === 0) {
      return {
        msg: 'LOGIN FAIL'
      }
    }
  } catch (e) {
    console.log('doctors',e)
  }

  var nowDate = new Date()
  console.log(nowDate)
  // openid 一天过期
  nowDate.setDate(nowDate.getDate() + 1)
  console.log(nowDate)
  try {
    res = await db.collection('active')
    .add({
      data: {
        expireDate: nowDate,
        actor: 'doctor',
        id: event.id,
        openid: openid
      }
    })
  } catch (e) {
    console.log('active', e)
  }

  return {
    msg: 'SUCCESS'
  }
}