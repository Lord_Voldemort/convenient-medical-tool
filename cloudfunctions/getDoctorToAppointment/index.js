// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getDoctorList(res) {
  return res.data
}
// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event)
  let res = await db.collection('doctors')
  .where({
    hospital: event.hospital,
    department: event.department
  }).get()

  let doctorList = await getDoctorList(res)

  return {
    doctorList: doctorList
  }
}