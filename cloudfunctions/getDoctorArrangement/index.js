// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getDoctorArrangement (res) {
  return {
    arrangeList: res.data
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  let res = await db.collection('arrangement')
  .where({
    doctorId: event.doctorId,
    date: event.date,
    period: event.period
  }).get()

  let ans = await getDoctorArrangement(res)
  return ans
}