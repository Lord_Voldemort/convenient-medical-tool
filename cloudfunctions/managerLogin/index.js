// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
async function isExisted (res) {
  
  return res.data.length !== 0
}
// 云函数入口函数
exports.main = async (event, context) => {

  let res = await db.collection('managers')
  .where({
    id: event.id,
    password: event.password
  }).get()

  let ans = await isExisted(res)
  return {
    msg: ans ? 'EXIST' : 'NOT EXIST'
  }
  
}