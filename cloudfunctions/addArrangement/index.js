// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const shiftMap = {
  'am': {
    '8:30-9:30': '0',
    '9:30-10:30': '1',
    '10:30-11:30': '2'
  },
  'pm': {
    '14:30-15:30': '0',
    '15:30-16:30': '1',
    '16:30-17:30': '2'
  }
}
function unifyShift (period, shift) {
  return shiftMap[period][shift]
}
// 云函数入口函数
exports.main = async (event, context) => {
  let res 
  let shift = unifyShift(event.period, event.shift)
  try {
    res = await db.collection('arrangement')
      .add({
        data: {
          date: event.date,
          doctorId: event.doctorId,
          period: event.period,
          shift: shift
        }
      })
  } catch (e) {
    console.log(e)
  }
  
}