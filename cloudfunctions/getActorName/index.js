// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getActorName(res) {
  return res.data[0].name
}
// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event)
  let res
  switch (event.actor) {
    case 'patient':
      res = await db.collection('patients')
      .where({
        openid: event.actorId
      }).get()
      break
    case 'doctor':
      res = await db.collection('doctors')
      .where({
        id: event.actorId
      }).get()
      break
  }
  console.log(res)
  let name = await getActorName(res)

  return name
}