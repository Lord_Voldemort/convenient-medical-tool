// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  let res
  try {
    res = await db.collection('active')
    .where({
      openid: openid
    }).remove()
  } catch (e) {
    console.log(e)
  }
  
  if (event.actor === 'patient') {
    try {
      res = await db.collection('patients')
      .where({
        openid: openid
      }).remove()
    } catch (e) {
      console.log(e)
    }
  }
  
}