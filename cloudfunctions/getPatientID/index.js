// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getPatientID (res) {
  return {
    idcard: res.data[0].idcard
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  let res = await db.collection('patients')
  .where({
    openid: event.openid
  }).get()

  let ans = await getPatientID(res)
  return ans
}