// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID

  let data
  switch (event.attribute) {
    case 'name':
      data = {
        name: event.value
      }
      break
    case 'phone':
      data = {
        phone: event.value
      }
      break
    case 'idcard':
      data = {
        idcard: event.value
      }
      break
  }
  let res 
  try {
    res = await db.collection('patients')
    .where({
      openid: openid
    }).update({
      data: data
    })
  } catch (e) {
    console.log(e)
    return {
      msg: 'UPDATE FAIL'
    }
  }
  return {
    msg: 'SUCCESS'
  }
}