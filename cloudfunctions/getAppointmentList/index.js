// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()


async function getAppointmentList (tempList) {
  for (var index in tempList)
  {
    console.log(tempList[index])
    let response = await db.collection('arrangement')
    .where({
      _id: tempList[index].arrangementId
    }).get()

    let arrangementInfo = await getArrangementInfo(response)

    response = await db.collection('patients')
    .where({
      idcard: tempList[index].patientId
    }).get()

    let patientName = await getPatientName(response)

    response = await db.collection('doctors')
    .where({
      _id: tempList[index].doctorId
    }).get()

    let doctorName = await getDoctorName(response)

    tempList[index] = Object.assign(arrangementInfo, patientName, doctorName, tempList[index])
  }
  return tempList
}

async function getArrangementInfo (res) {
  return {
    date: res.data[0].date,
    period: res.data[0].period,
    shift: res.data[0].shift
  }
}

async function getPatientName (res) {
  return {
    patientName: res.data[0].name
  }
}

async function getDoctorName (res) {
  return {
    doctorName: res.data[0].name
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event.appointmentList)

  let ans = await getAppointmentList(event.appointmentList)
  return ans
}