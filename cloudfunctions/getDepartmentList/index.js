// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getDepartmentList(res) {
  return res.data
}
// 云函数入口函数
exports.main = async (event, context) => {
  let res = await db.collection('departments')
  .where({
    hospital: event.hospital
  }).get()

  let departmentList = await getDepartmentList(res)
  return {
    departmentList: departmentList
  }
}