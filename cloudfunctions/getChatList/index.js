// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
function getTimeFromDate (milliseconds) {
  var date = new Date(milliseconds)
  var hh = date.getHours()
  var min = date.getMinutes()
  hh = (hh<10)?("0"+hh):hh
  min = (min<10)?("0"+min):min
  var format = hh + ':' + min
  return format
}

// 云函数入口函数
exports.main = async (event, context) => {
  console.log('event', event)
  let actorList = {}
  let isDoctor = event.actor === 'doctor'
  for (var item of event.message)
  {
    var time = getTimeFromDate(item.sendTimeTS)
    var actorId = isDoctor ? item.patientId : item.doctorId
    'use strict'
    Object.defineProperty(actorList, actorId, {value: {
      textContent: item.msgType === 'image' ? "[图片]" : item.textContent,
      sendTime: time,
      patientId: item.patientId,
      doctorId: item.doctorId
    }, configurable: true, writable: true, enumerable: true} )
  }
  
  for (const [memberId, value] of Object.entries(actorList))
  {
    await cloud.callFunction({
      name: 'getActorName',
      data: {
        actorId: memberId,
        actor: event.actor === 'doctor' ? 'patient' : 'doctor'
      }
    }).then((res) => {
      value['name'] = res.result
    })
    
  }
  return actorList
}