// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getDepartmentInfo(res) {
  console.log(res)
  return {
    hospital: res.data.hospital,
    department: res.data.department
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  console.log(event)
  let res = await db.collection('departments')
  .doc(event.departmentId).get()

  let ans = getDepartmentInfo(res)

  return ans
}