// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getDoctorInfo (res) {
  return {
    department: res.data[0].department,
    hospital: res.data[0].hospital,
    job: res.data[0].job,
    name: res.data[0].name
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  let res = await db.collection('doctors')
  .where({
    _id: event.doctorId
  }).get()
  let ans = await getDoctorInfo(res)

  return ans
}