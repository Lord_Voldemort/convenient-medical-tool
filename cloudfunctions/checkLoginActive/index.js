// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

function isExpire (expireDate) {
  var nowDate = new Date()
  return nowDate > expireDate
}

async function isActive (res) {
  console.log(res)
  if (res.data.length === 0) {
    return {
      isActive: false
    }
  } else {
    var isExpired = isExpire(res.data[0].expireDate)
    return {
      isActive: true,
      isExpire: isExpired,
      actor: res.data[0].actor,
      id: res.data[0].id
    }
  }
}

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const openid = wxContext.OPENID
  
  let res = await db.collection('active')
  .where({
    openid: openid
  }).get()

  let ans = await isActive(res)
  if (ans.isActive) {
    // 通道不一样
    if (ans.actor !== event.actor) {
      return {
        msg: 'NOT ACTIVE'
      }
    }
    if (ans.isExpire) {
      // openid 过期，删除数据库中的记录
      if (ans.actor === 'patient') {
        try {
          let response = await db.collection('patients')
          .where({
            openid: openid
          }).remove()

          response = await db.collection('active')
          .where({
            openid: openid
          }).remove()
        } catch (e) {
          console.log(e)
        }
      } else if (ans.actor === 'doctor') {
        let response = await db.collection('active')
        .where({
          openid: openid
        }).remove()
      }
      
      return {
        msg: 'EXPIRE'
      }
    } else {
      // 更新 expireDate
      let nowDate = new Date()
      nowDate.setDate(nowDate.getDate() + 1)

      try {
        res = await db.collection('active')
        .where({
          openid: openid
        }).update({
          data: {
            expireDate: nowDate
          }
        })
      } catch (e) {
        console.log(e)
      }

      if (ans.actor === 'patient') {
        return {
          msg: 'NOT EXPIRE',
          openid: openid
        }
      } else {
        return {
          msg: 'NOT EXPIRE',
          id: ans.id
        }
      }
    }
  } else {
    return {
      msg: 'NOT ACTIVE'
    }
  }
}