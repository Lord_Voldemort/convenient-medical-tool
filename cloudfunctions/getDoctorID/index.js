// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()

async function getDoctorID(res) {
  return {
    doctorId: res.data[0]._id
  }
}
// 云函数入口函数
exports.main = async (event, context) => {
  let res = await db.collection('doctors')
  .where({
    id: event.doctorId
  }).get()

  let ans = await getDoctorID(res)
  return ans
}